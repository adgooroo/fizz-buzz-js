# Fizz Buzz


## Description:

Complete the function that returns an array with the numbers from 1 to n. But for multiples of three use “Fizz” instead of the number and for the multiples of five use “Buzz”. For numbers which are multiples of both three and five use “FizzBuzz”.

```javascript
var result = fizzBuzz(20);

console.log(result);

// Output:
// [1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz", 11, "Fizz", 13, 14, "FizzBuzz", 16, 17, "Fizz", 19, "Buzz"]

```

## Installation:

```bash
$ npm install
$ npm start
```

## To Run Tests"
```bash
$ npm test
```