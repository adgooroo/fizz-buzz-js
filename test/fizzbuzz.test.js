import fizzBuzz from '../fizzBuzz';

describe('fizzBuzz', () => {
	it('returns an array type', () => {
		const result = fizzBuzz(1);
		assert(Array.isArray(result));
	});

	it('returns an empty array when n is undefined', () => {
		const result = fizzBuzz();
		expect(result).to.deep.eq([]);
	});

	it('returns an empty array when n is 0', () => {
		const result = fizzBuzz(0);
		expect(result).to.deep.eq([]);
	});

	it('returns an fizzbuzz array with 20 values', () => {
		const result = fizzBuzz(20);
		const expect = [1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz", 11, "Fizz", 13, 14, "FizzBuzz", 16, 17, "Fizz", 19, "Buzz"];
		chai.expect(result).to.deep.eq(expect);
	});

	it('returns an fizzbuzz array with 1 values', () => {
		const result = fizzBuzz(1);
		const expect = [1];
		chai.expect(result).to.deep.eq(expect);
	});

	it('throws an error for values less than 0', () => {
		const shouldThrow = () => fizzBuzz(-100);
		expect(shouldThrow).to.throw();
	});

	it('throws for a non-number n value', () => {
		const throwWithString = () => fizzBuzz("unhappy");
		const throwWithArr = () => fizzBuzz(["unhappy"]);
		const throwWithObj = () => fizzBuzz({ a: "unhappy" });
		expect(throwWithString).to.throw();
		expect(throwWithArr).to.throw();
		expect(throwWithObj).to.throw();
	});
});